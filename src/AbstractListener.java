
/**
 * Classe parente de tous les listeners qui permet de centraliser
 * leurs caracteristiques communes
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
public class AbstractListener {
	
	// mode où on ajoute des points à une figure en construction
	public static final int EDIT_MODE = 1;
	// mode où on rempli des figures de couleur
	public static final int FILLING_MODE = 2;
	// mode en cours
	private static int CURRENT_MODE ;

	/**
	 * Methode qui vérifie si l'action donnée en paramètre est la construction d'une des 4 figures
	 *
	 * @param action L'action requise par l'évènement 
	 */
	public static boolean isFigureAction(String action) {
		return (
			   action == Configuration.TRIANGLE
			|| action == Configuration.SQUARE
			|| action == Configuration.CIRCLE
			|| action == Configuration.POLYGON
		);
	}

	/**
	 * Renvoit le mode courant
	 * @return int Le mode courant
	 */
	public static int getMode() { return AbstractListener.CURRENT_MODE; }
	
	/**
	 * Met à jour le mode courant
	 * @param mode La nouvelle valeur du mode courant
	 */
	public static void setMode(int mode) { AbstractListener.CURRENT_MODE = mode; }
	
	/**
	 * Méthode qui remet le mode édition
	 */
	public static void resetMode() { AbstractListener.CURRENT_MODE = EDIT_MODE; }
}
