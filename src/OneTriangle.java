import java.awt.Color;


/**
 * Classe permettant de générer des triangles
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
@SuppressWarnings("serial")
public class OneTriangle extends OnePolygon {
	
	/**
	 * constructeur d'un triangle
	 */
	public OneTriangle(Color c, boolean fill) {
		super(c, fill);
		this.nb_point_final = 3;
	}
	
	/**
	 * constructeur par copie
	 */
	public OneTriangle(FigureGeom f) {
		super(f);
		this.nb_point_final = 3;
	}
	
}
