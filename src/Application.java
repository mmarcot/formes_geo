import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.*;


/**
 * Classe principale qui créé l'interface utilisateur
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
@SuppressWarnings("serial")
public class Application extends JFrame {
	
	// La barre d'outils
	private static JToolBar toolbar = null;
	// La zone de dessin
	private static DrawPanel drawing_zone  = null;

	/**
	 * Méthode principale de lancement de l'application
	 */
	public static void main(String[] args) {
		// création de la fenetre :
		JFrame window = new JFrame(Configuration.WINDOW_NAME);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(true);
		JPanel wrapper = new JPanel();
		window.setContentPane(wrapper);
		wrapper.setLayout(new BorderLayout());

		// Création de la zone de dessin :
		drawing_zone = new DrawPanel();
		DrawPanelListener panel_listener = new DrawPanelListener();
		drawing_zone.addMouseListener(panel_listener);
		drawing_zone.addMouseMotionListener(panel_listener);
		drawing_zone.setBackground(Color.WHITE);
		wrapper.add(drawing_zone);

		// Ajout de la barre d'outil :
		toolbar = new JToolBar();
		wrapper.add(toolbar, BorderLayout.NORTH);
		toolbar.setFloatable(false);

		// Ajout des boutons à la barre d'outil :
		toolbar.addSeparator();
		toolbar.add(new JLabel("Dessiner : "));
		addButtonToToolBar(Configuration.SQUARE, Configuration.RECTANGLE_TOOLTIP);
		addButtonToToolBar(Configuration.CIRCLE, Configuration.CIRCLE_TOOLTIP);
		addButtonToToolBar(Configuration.TRIANGLE, Configuration.TRIANGLE_TOOLTIP);
		addButtonToToolBar(Configuration.POLYGON, Configuration.POLYGON_TOOLTIP);
		addButtonToToolBar(Configuration.DELETE, Configuration.DELETE_TOOLTIP);

		toolbar.addSeparator();
		addButtonToToolBar(Configuration.COPY, Configuration.COPY_TOOLTIP);
		toolbar.addSeparator();
		toolbar.add(new JLabel("Colorier : "));
		// créé les boutons poussoirs et les active par défaut (pour pouvoir utiliser la couleur du background lorsqu'il sont activés)
		addToggleButtonToToolBar(Configuration.COLORS, Configuration.FILL_TOOLTIP).getModel().setSelected(true);
		addToggleButtonToToolBar(Configuration.TRANSPARENT, Configuration.TRANSP_TOOLTIP).getModel().setSelected(true);
		toolbar.addSeparator();

		// Création des menu :
		JMenuBar menu_bar = new JMenuBar();
		window.setJMenuBar(menu_bar);
		// Menu Fichier :
		JMenu file_menu = new JMenu("Fichier");
		menu_bar.add(file_menu);
		// Sauvegarder :
		addToMenu(file_menu, "save", Configuration.SAVE);
		// Exporter en JPG :
		addToMenu(file_menu, "export", Configuration.EXPORT);
		// Charger :
		addToMenu(file_menu, "load", Configuration.LOAD);
		// Quitter :
		addToMenu(file_menu, "exit", Configuration.EXIT);
		// Menu Edition :
		JMenu edition_menu = new JMenu("Edition");
		menu_bar.add(edition_menu);
		// Annuler :
		addToMenu(edition_menu, "undo", Configuration.UNDO);
		// Refaire :
		addToMenu(edition_menu, "redo", Configuration.REDO);
		// Menu ?
		JMenu help_menu = new JMenu("?");
		menu_bar.add(help_menu);
		//A propos
		addToMenu(help_menu, "a propos", Configuration.A_PROPOS);
		// Version
		addToMenu(help_menu, "version", Configuration.VERSION);
		// Mettre a jour
		addToMenu(help_menu, "mettre a jour", Configuration.UPDATE);

		// Placement de la fenêtre au milieu de l'écran :
		window.pack();
		Dimension screen_dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int pos_init_x = (screen_dimension.width - window.getSize().width)/2;
		int pos_init_y = (screen_dimension.height - window.getSize().height)/2;
		window.setLocation(pos_init_x, pos_init_y);
		window.setVisible(true);
	}

	/**
	 * Ajoute un sous-menu au JMenu passé en paramètre
	 *
	 * @param edition_menu
	 * @param string
	 */
	private static void addToMenu(JMenu menu, String sub_menu_kind, String sub_menu_label) {
		ImageIcon sub_menu_icon = new ImageIcon("../img/"+sub_menu_kind+".png", sub_menu_kind+" icon");
		JMenuItem sub_menu = new JMenuItem(sub_menu_label, sub_menu_icon);
		sub_menu.addActionListener(new MenuListener(drawing_zone));
		menu.add(sub_menu);
	}

	/**
	 * Ajoute un JButton à la toolbar de l'application
	 *
	 * @param button_kind
	 * @param label
	 */
	private static JButton addButtonToToolBar(String button_kind, String label) {
		ImageIcon square_icon = new ImageIcon("../img/"+button_kind+".png", button_kind+" icon");
		JButton b = new JButton(square_icon);
		b.addActionListener(new ToolBarListener(drawing_zone));
		b.setToolTipText(label);
		b.setActionCommand(button_kind);
		toolbar.add(b);
		return b;
	}

	/**
	 * Ajoute un JToggleButton à la toolbar de l'application
	 *
	 * @param button_kind
	 * @param label
	 */
	private static JToggleButton addToggleButtonToToolBar(String button_kind, String label) {
		ImageIcon square_icon = new ImageIcon("../img/"+button_kind+".png", button_kind+" icon");
		JToggleButton b = new JToggleButton(square_icon);
		b.addActionListener(new ToolBarListener(drawing_zone));
		b.setToolTipText(label);
		b.setActionCommand(button_kind);
		toolbar.add(b);
		return b;
	}

}
