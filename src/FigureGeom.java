import java.util.ArrayList;
import java.awt.Color;
import java.awt.Point;


/**
 * Classe qui modélise des figures géometrique sur un plan 2D
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 * @see javadoc Observable
 */
@SuppressWarnings("serial")
abstract class FigureGeom implements java.io.Serializable {
	
	// Attributs
	protected Color color;
	protected boolean filled;
	protected boolean selected;
	protected boolean completed;
	protected int nb_point_current;
	protected int nb_point_final;
	protected ArrayList<PointArea> input_points;
	protected ArrayList<Point> saved_points;


	/**
	 * Constructeur d'une figure géometrique
	 * @param c Couleur de la figure
	 * @param fill remplie ou non
	 */
	protected FigureGeom(Color c, boolean fill) {
		this.completed = false;
		this.selected = false;
		this.color = c;
		this.filled = fill;
		this.nb_point_current = 0;
		this.input_points = new ArrayList<PointArea>();
		this.saved_points = new ArrayList<Point>();
	}

	/**
	 * Constructeur par copie
	 * @param fg la figure à copier
	 */
	protected FigureGeom(FigureGeom fg) {
		this.completed = fg.completed;
		this.color = fg.color;
		this.filled = fg.filled;
		this.nb_point_current = fg.nb_point_current;
		this.input_points = new ArrayList<PointArea>();
		this.saved_points = new ArrayList<Point>();

		// on copie tous les points en leur assignant une nouvelle référence
		// pour faire une "deep copy"
		for( PointArea pa : fg.getInputPoints() ) {
			this.input_points.add(new PointArea(pa.x, pa.y));
		}
		for( Point p : fg.getSavedPoints() ) {
			this.saved_points.add(new Point(p.x, p.y));
		}
	}

	/**
	 * Methode qui translate la figure entière
	 * @param move_x Mouvement en abscisse
	 * @param move_y Mouvement en ordonnée
	 */
	public void translate(int move_x, int move_y) {

		// on translate tous les inputs points :
		for(int i = 0; i < this.input_points.size(); i++)
			this.input_points.get(i).translate(move_x, move_y);

		// ... puis on génère les points de mémorisation :
		this.genSavedPoints();
	}

	/**
	 * Ajoute un point sélectionné par l'utilisateur à la figure en cours de construction
	 * @param point Le nouveau point choisi par l'utilisateur
	 */
	public void addPoint(Point p) {
		this.nb_point_current++;

		this.input_points.add(new PointArea(p.x, p.y));

		// si on construit un polgone, on regarde si le point cliqué est dans le
		// premier PointArea. Si oui, la figure est terminée, sinon on continue
		if (this.getClass().getName() == "OnePolygon" && this.input_points.size() > 1) {
			PointArea first = this.input_points.get(0);

			if (first.contains(p)) {
				this.completed = true;
				// on évite les points en doublons (suppression du dernier)
				this.input_points.remove(p);
				this.genSavedPoints();
			}
		} else {
			if( this.nb_point_current == this.nb_point_final) {
				this.completed = true;
				this.genSavedPoints();
			} else {
				this.completed = false;
			}
		}
	}

	/**
	 * Methode qui va générer les points de memorisation en fonction
	 * des input points (à surcharger si les points de mémorisation sont
	 * différents des points de saisie
	 */
	protected void genSavedPoints() {

		// on vide la liste :
		this.saved_points.clear();

		// puis on la remplis correctement :
		for(final PointArea p : this.input_points) {
			this.saved_points.add(new Point(p.x, p.y));
		}
	}

	/**
	 * Permet de savoir si un point est contenu dans une figure
	 * doit être surchargée dans les sous-classes
	 * @param point le point à localiser
	 */
	abstract public boolean contains(Point point);

	//#################################### GETTER AND SETTER ##############################################

	/**
	 * Methode qui permet de récupérer la liste des points de mémorisation
	 * @return ArrayList de Point
	 */
	public ArrayList<Point> getSavedPoints() {
		return this.saved_points;
	}

	/**
	 * Change la couleur de la figure courante
	 * @param c Nouvelle couleur
	 */
	public void setColor(Color c) {
		this.color = c;
	}

	/**
	 * Change la couleur de la figure courante
	 * @param c Nouvelle couleur
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * Permet de remplir une figure
	 * @param v vrai ou faux
	 */
	public void setFilled(boolean v) {
		this.filled = v;
	}

	/**
	 *
	 * @param v vrai ou faux
	 */
	public boolean isFilled() {
		return this.filled;
	}

	/**
	 * Methode qui dit si une figure est selectionnée ou non
	 * @return vrai si selectionnée, faux sinon
	 */
	public boolean isSelected() {
		return this.selected;
	}

	/**
	 * Permet de savoir si tous les points ont été saisis
	 * @return vrai si tous saisis, faux sinon
	 */
	public boolean isCompleted() {
		return this.completed;
	}

	/**
	 * Methode qui permutte l'attribut selected
	 * @param value	La nouvelle valeur (vrai ou faux)
	 */
	public void setSelected(boolean value) {
		this.selected = value;
	}

	/**
	 * Methode qui permet de changer le point d'index n de position
	 * @param index_point le numero du point
	 * @param dx Distance en abscisse
	 * @param dy Distance en ordonnée
	 */
	public void setPoint(int index_point, int dx, int dy) {
		this.input_points.get(index_point).translate(dx, dy);
		this.genSavedPoints();
	}

	// getter for input_points
	public ArrayList<PointArea> getInputPoints() { return this.input_points; }

}

