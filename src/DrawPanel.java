import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 * Classe qui définit la zone de dessin (JPanel)
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
@SuppressWarnings("serial")
public class DrawPanel extends JPanel {

	// Liste des figures supprimées
	private ArrayList<FigureGeom> tab_deleted_figures;
	// Liste de figure actuellement affichée
	private ArrayList<FigureGeom> tab_figures;
	// Liste de listes permettant de undo/redo
	private ArrayList<ArrayList<FigureGeom>> saved_statements;
	// Index de la liste actuelle dans saved_statements
	private int tab_index;
	// Contexte graphique
	private Graphics graphics = null;
	// figure en cours de construction (edit mode)
	private FigureGeom building_figure = null;
	// couleure en cours sélection (filling mode)
	private Color chosen_color = null;

	/**
	 * Constructeur d'un DrawPanel de type JPanel
	 */
	public DrawPanel() {
		this.tab_figures = new ArrayList<FigureGeom>();
		this.tab_deleted_figures = new ArrayList<FigureGeom>();
		this.tab_index = 0;
		this.setPreferredSize(new Dimension(Configuration.DRAW_PANEL_WIDTH,
											Configuration.DRAW_PANEL_HEIGHT));

		this.saved_statements = new ArrayList<ArrayList<FigureGeom>>();
		this.saveCurrentStatement();
	}
	
	
	/**
	 * Méthode qui permet d'exporter un JPanel en image
	 */
	public void exportToJpg() {
		BufferedImage bi = createImage();
		String file_name = JOptionPane.showInputDialog("Nom du fichier : ");
		file_name = new String(file_name + ".jpg");

		File outputfile = new File(file_name);
		try {
	    ImageIO.write(bi, "jpg", outputfile);
	    JOptionPane.showMessageDialog(this, "Fichier " + file_name + " exporté !", "Bravo", JOptionPane.INFORMATION_MESSAGE);
    } 
		catch (IOException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Erreur !", JOptionPane.ERROR_MESSAGE);
    }
		
	}
	
	
	/**
	 * Convertion du JPanel en BufferedImage
	 *
	 * @param panel JPanel à convertir
	 * @return la buffered image
	 */
	public BufferedImage createImage() {
    int w = this.getWidth();
    int h = this.getHeight();
    BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    Graphics2D g = bi.createGraphics();
    this.paint(g);
    
    return bi;
}

	/**
	 * Met l'état du JPanel courant dans la liste de sauvegarde
	 */
	public void saveCurrentStatement() {
		ArrayList<FigureGeom> cp_curr = new ArrayList<FigureGeom>();
		this.saved_statements.add(0, new ArrayList<FigureGeom>(tab_figures));

		for( FigureGeom f : tab_figures ) {
			if( f instanceof OneTriangle )
				cp_curr.add(new OneTriangle(f));
			else if( f instanceof OneRectangle )
				cp_curr.add(new OneRectangle(f));
			else if( f instanceof OnePolygon )
				cp_curr.add(new OnePolygon(f));
			else if( f instanceof OneCircle )
				cp_curr.add(new OneCircle(f));
		}
		saved_statements.add(0, cp_curr);

		// si la taille de la liste dépasse la taille voulue alors
		// on supprime le plus ancien statement :
		if( saved_statements.size() > Configuration.SIZE_UNDO_REDO ) {
			saved_statements.remove(saved_statements.size()-1);
		}
	}

	/**
	 * Méthode qui ajoute une FigureGeom à la liste
	 * @param figure La figure à ajouter
	 */
	public void addFigure(FigureGeom figure) {
		this.tab_figures.add(figure);
		this.repaint();
	}
	
	/**
	 * Permet de dupliquer une figure géometrique
	 *
	 * @param f Figure à dupliquer
	 */
	public void duplicateFigure(FigureGeom f) {
		if( f instanceof OneTriangle ) {
			OneTriangle nf = new OneTriangle(f);
			nf.translate(5, 5);
			tab_figures.add(nf);
		}
		else if( f instanceof OneRectangle ) {
			OneRectangle nf = new OneRectangle(f);
			nf.translate(5, 5);
			tab_figures.add(nf);
		}
		else if( f instanceof OnePolygon ) {
			OnePolygon nf = new OnePolygon(f);
			nf.translate(5, 5);
			tab_figures.add(nf);
		}
		else if( f instanceof OneCircle ) {
			OneCircle nf = new OneCircle(f);
			nf.translate(5, 5);
			tab_figures.add(nf);
		}
		
		this.repaint();
	}
	
	

	/**
	 * Méthode qui enleve une FigureGeom à la liste
	 * ajoute la figure enlever dans une liste pour la suppression
	 * déselectionne la figure
	 */
	public void deleteFigure(FigureGeom figure) {
		this.tab_figures.remove(figure);
		this.tab_deleted_figures.add(figure);
		// passe l'avant-dernière figure en selected
		this.setFigureAsSelected(this.getLastFigure());

		this.repaint();
	}

	
	
	/**
	 * Méthode qui rend sélectionnée la figure donnée en paramètre et qui dé-sélectionne toutes les autres
	 */
	public boolean setFigureAsSelected(FigureGeom figure) {
		boolean success = false;
		int index = this.tab_figures.indexOf(figure);

		if (index != -1) {
			// si la liste n'est pas vide, on parcourt toutes les formes de la liste et on les dé-sélectionne
			if (!this.tab_figures.isEmpty()) {
				for (int i = 0; i < this.tab_figures.size(); i++) {
					this.tab_figures.get(i).setSelected(false);
				}
			}
			// puis on met la nouvelle figure sélectionnée à la fin de la liste et on la met en séléctionnée
			this.tab_figures.remove(figure);
			this.tab_figures.add(figure);
			figure.setSelected(true);

			success = true;
		}
		// on repaint pour faire apparaitre les nouveau carré de selection 
		// lorsqu'on change de figure selectionnée via MousePressed
		repaint();
		return success;
	}

	
	
	/**
	 * Méthode façade qui renvoit la dernière figure de la pile
	 */
	public FigureGeom getSelectedFigure() {
		return this.getLastFigure();
	}

	/**
	 * Méthode qui renvoit la dernière figure de la pile
	 */
	public FigureGeom getLastFigure() {
		int index = this.tab_figures.size() - 1;
		FigureGeom figure = null;

		if (!this.tab_figures.isEmpty()) {
			figure = this.tab_figures.get(index);
		}

		return figure;
	}
	
	

	/**
	 * Méthode qui permet de dessiner le contenu du JPanel
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.graphics = g;

		ArrayList<FigureGeom> figures = this.tab_figures;
		FigureGeom figure;
		g.setColor(Color.black);

		for (int i = 0; i < figures.size(); i++) {
			figure = figures.get(i);
			// dessine les traits de la figure
			this.drawFigure(figure);
			
			// dessine les PointArea si la figure est sélectionnée
			if (figure.isSelected()) {
				this.drawPointsArea(figure);
			}
		}

		// dessine les PointArea au fur et à mesure de la construction de la figure
		if (AbstractListener.getMode() == AbstractListener.EDIT_MODE) {
			figure = this.building_figure;
			this.drawPointsArea(figure);
		}
	}
	
	
	/**
	 * Méthode qui dessine les petits carrés de selection
	 */
	private void drawPointsArea(FigureGeom figure) {
		this.graphics.setColor(Color.black);
		ArrayList<PointArea> input_points = new ArrayList<PointArea>();
		
		if (figure != null) {
			input_points = figure.getInputPoints();
			for (int l = 0; l < input_points.size(); l++) {
				Rectangle area = input_points.get(l).getDraggableArea();
				this.graphics.drawRect((int) area.getX(), (int) area.getY(), (int) area.getWidth(), (int) area.getHeight());
			}
		}
	}
	

	/**
	 * Méthode qui dessine les figures géométriques
	 */
	private void drawFigure(FigureGeom figure) {
		ArrayList<Point> saved_points = figure.getSavedPoints();
		this.graphics.setColor(figure.getColor());

		if (figure instanceof OneCircle) {
			Point p1 = saved_points.get(0);
			Point p2 = saved_points.get(1);
			int dist = (int) Math.sqrt(Math.pow((p2.getX() - p1.getX()), 2) + Math.pow((p2.getY() - p1.getY()), 2));
			int p3_x = (int) (p1.getX() - dist);
			int p3_y = (int) (p1.getY() - dist);
			
			if (figure.isFilled()) {
			// si elle est remplie, dessine l'intérieur
				// this.graphics.setColor(figure.getColor());
				this.graphics.fillOval(p3_x, p3_y, 2*dist, 2*dist);
			}
			// dessin le contour de la figure
			this.graphics.setColor(Color.black);
			this.graphics.drawOval(p3_x, p3_y, 2*dist, 2*dist);
			
		} else {
			Polygon polygon = new Polygon();

			for (int j = 0; j < saved_points.size(); j++) {
				int point_area_x = (int) saved_points.get(j).getX();
				int point_area_y = (int) saved_points.get(j).getY();
				polygon.addPoint(point_area_x, point_area_y);
			}
		
			if (figure.isFilled()) {
				// si elle est remplie, dessine l'intérieur
				// this.graphics.setColor(figure.getColor());
				this.graphics.fillPolygon(polygon);
			}
			// dessin le contour de la figure
			this.graphics.setColor(Color.black);
			this.graphics.drawPolygon(polygon);	
		}
	}
	
	
	/**
	 * Méthode qui permet d'annuler une action
	 */
	public void undo() {
		if(tab_index < 9) {
			tab_index++;
			refreshCurrentList();
		}
	}
	

	/**
	 * Methode qui permet d'annuler une annulation d'action
	 */
	public void redo() {
		if(tab_index > 0) {
			tab_index--;
			refreshCurrentList();
		}
	}
	
	
	/**
	 * Méthode qui met à jour la liste courante de FigureGeom sur le JPanel
	 * en fonction du tab_index (pour undo/redo)
	 */
	private void refreshCurrentList() {
		tab_figures = new ArrayList<FigureGeom>(saved_statements.get(tab_index));
		repaint();
  }
	
	
	/**
	 * Permet de sauvegarder le JPanel
	 */
	public void savePanel() {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("sauvegarde_du_panel.sav"));
			out.writeObject(tab_figures);
			out.close();
			JOptionPane.showMessageDialog(this, Configuration.SAVED_MESS);
		} catch( IOException e ) {
			JOptionPane.showMessageDialog(this, Configuration.ERR_LOAD);
		}
	}
	
	
	/**
	 * Permet de charger un DrawPanel sauvegardé
	 */
	@SuppressWarnings("unchecked")
	public void loadPanel() {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("sauvegarde_du_panel.sav"));
			this.tab_figures = (ArrayList<FigureGeom>) in.readObject();
			in.close();
			repaint();
		}
		catch( ClassNotFoundException e1 ) {
			JOptionPane.showMessageDialog(this, Configuration.ERR_LOAD);
		}
		catch( IOException e2 ) {
			JOptionPane.showMessageDialog(this, Configuration.ERR_LOAD);
		}
	}

	// getter and setter
	public void setTabIndex(int v) { this.tab_index = v; }
	public ArrayList<FigureGeom> getTabFigures() { return this.tab_figures; }
	public ArrayList<FigureGeom> getDelFigures() { return this.tab_deleted_figures; }
	// getter and setter for building_figure
	public FigureGeom getBuildingFigure() { return this.building_figure; }
	public void setBuildingFigure(FigureGeom figure) { this.building_figure = figure; }
	// getter and setter for chosen_color
	public Color getChosenColor() { return this.chosen_color; }
	public void setChosenColor(Color chosen_color) { this.chosen_color = chosen_color; }
}
