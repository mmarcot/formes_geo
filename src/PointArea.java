import java.awt.Point;
import java.awt.Rectangle;


/**
 * Gère les zones de sélection des points
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
@SuppressWarnings("serial")
public class PointArea extends Point {
	
	// attributs
	private Rectangle draggable_area;
	
	/**
	 * Constructeur
	 * 
	 * @param px Abscisse centre du carré 
	 * @param py Ordonnée centre du carré
	 */
	public PointArea(int px, int py) {
		int s = Configuration.POINTAREA_SIZE;
		
		this.draggable_area = new Rectangle((px-s/2), (py-s/2), s, s);
		this.x = px;
		this.y = py;
	}
	
	/**
	 * renvoit true si le point est contenu dans la zone sélectionnable
	 * 
	 * @param point Point à tester
	 * @return vrai si contenu dedans, faux sinon
	 */
	public boolean contains(Point point) {
		return (this.draggable_area.contains(point));
	}
	
	/**
	 * Methode permettant de translater un PointArea
	 * @param dx Distance en abscisse
	 * @param dy Distance en ordonnée
	 */
	@Override
	public void translate(int dx, int dy) {
		super.translate(dx,dy);
		this.draggable_area.translate(dx, dy);
	}

	// getter for draggable_area
	public Rectangle getDraggableArea() { return this.draggable_area; }
}
