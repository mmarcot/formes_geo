import java.awt.Color;
import java.awt.Point;


/**
 * Classe permettant de générer des polygones
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
@SuppressWarnings("serial")
public class OnePolygon extends FigureGeom {

	/**
	 * Constructeur d'un polygone
	 */
	public OnePolygon(Color c, boolean fill) {
		super(c, fill);
		this.nb_point_final = Integer.MAX_VALUE;
	}
	
	/**
	 * Constructeur par copie
	 */
	public OnePolygon(FigureGeom f) {
		super(f);
		this.nb_point_final = Integer.MAX_VALUE;
	}

	/**
	 * Methode qui permet de savoir si un point est contenu dans un polygone ou non
	 * @param test Le point à tester
	 * @return true si le point est à l'interieur, false sinon
	 */
	@Override
	public boolean contains(Point test) {
		int i, j;
		boolean result = false;
		for (i = 0, j = saved_points.size() - 1; i < saved_points.size(); j = i++) {
			if ((saved_points.get(i).y > test.y) != (saved_points.get(j).y > test.y) &&
					(test.x < (saved_points.get(j).x - saved_points.get(i).x) * (test.y - saved_points.get(i).y) / (saved_points.get(j).y-saved_points.get(i).y) + saved_points.get(i).x)) {
				result = !result;
			}
		}
		return result;
	}

}
