import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;


/**
 * Classe auditrice qui gère les interactions avec le JMenu
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
public class MenuListener extends AbstractListener implements ActionListener, ItemListener {

	// La zone de dessin
	private DrawPanel drawing_zone;
	
	/**
	 * Constructeur
	 */
	public MenuListener(DrawPanel dz) {
	  this.drawing_zone = dz;
  }
	
	/**
	 * Methode qui implémente les actions à réaliser 
	 * lorsque l'utilisateur clique sur un élément du menu
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		
			case Configuration.SAVE :
				drawing_zone.savePanel();
				break;
			case Configuration.EXPORT :
				drawing_zone.exportToJpg();
				break;
			case Configuration.LOAD :
				drawing_zone.loadPanel();
				break;
			case Configuration.EXIT :
				System.exit(0);
				break;
			case Configuration.UNDO :
				drawing_zone.undo();
				break;
			case Configuration.REDO :
				drawing_zone.redo();
				break;
			case Configuration.A_PROPOS:
				JOptionPane.showMessageDialog(drawing_zone, Configuration.ABOUT_MES);
				break;
			case Configuration.VERSION :
				JOptionPane.showMessageDialog(drawing_zone, Configuration.VERS_MES);
				break;
			case Configuration.UPDATE :
				JOptionPane.showMessageDialog(drawing_zone, Configuration.UPDATE_MES);
				break;
			default :
				break;
		}
  }

	// Méthode non surchargée :
	public void itemStateChanged(ItemEvent arg0) {}

}
