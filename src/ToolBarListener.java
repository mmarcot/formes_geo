import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JColorChooser;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * Classe auditrice qui gère les interactions avec la toolbar
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
public class ToolBarListener extends AbstractListener implements ActionListener {
	
	// La zone de dessin
	private DrawPanel drawing_zone = null;

	/**
	 * Constructeur
	 */
	public ToolBarListener(DrawPanel drawing_zone) {
		this.drawing_zone = drawing_zone;
	}

	/**
	 * Methode qui gère les actions à réaliser lors d'un clic sur
	 * boutton de la toolbar.
	 * Gère les différents modes :
	 * 	- mode selection de figure
	 * 	- mode remplissage de figure (couleur)
	 * 	- mode ajout de point à une figure en construction
	 */
	@Override
  	public void actionPerformed(ActionEvent e) {
	  	String action_called = e.getActionCommand();

	  	if (AbstractListener.isFigureAction(action_called)) {
	  		AbstractListener.setMode(AbstractListener.EDIT_MODE);
	  	}

	  	switch (action_called) {
	  		case Configuration.BACK_TO_SELECT_MODE:
	  			AbstractListener.resetMode();
		  		break;
		  		
	  		case Configuration.COPY:
	  			drawing_zone.duplicateFigure(drawing_zone.getSelectedFigure());
	  			break;

		  	case Configuration.TRANSPARENT:
			  	final JToggleButton button1 = (JToggleButton) e.getSource(); 
		  		if (AbstractListener.getMode() == AbstractListener.FILLING_MODE) {
		  			AbstractListener.resetMode();
		  			drawing_zone.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		  			button1.getModel().setSelected(true);
		  		} else {
		  			AbstractListener.setMode(AbstractListener.FILLING_MODE);
		  			button1.getModel().setSelected(false);
		  			// on défini la couleur transparent
			  		Color transparent = new Color(0, 0, 0, 0);
			  		this.drawing_zone.setChosenColor(transparent);
			  		// on change le curseur
			  		this.setFillingCursor();
		  		}
		  		break;

		  	case Configuration.COLORS:	
			  	final JToggleButton button2 = (JToggleButton) e.getSource(); 
			  	// si le mode remplissage est deja actif, on le desactive
		  		if (AbstractListener.getMode() == AbstractListener.FILLING_MODE) {
		  			AbstractListener.resetMode();
            		drawing_zone.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		  			button2.getModel().setSelected(true);
		  		} else {
		  		// si le mode remplissage n'est pas actif, on l'active
			  		AbstractListener.setMode(AbstractListener.FILLING_MODE);
		  			button2.getModel().setSelected(false);
		  			
		  			// on créé la fenetre de choix des couleurs
		  			final JColorChooser color_chooser = new JColorChooser();
			  		color_chooser.getSelectionModel().addChangeListener(new ChangeListener() {
						public void stateChanged(ChangeEvent e) {
							Color col = color_chooser.getColor(); 
							drawing_zone.setChosenColor(col);
							button2.setBackground(col);
		                }
		            });
			  		JOptionPane.showMessageDialog(this.drawing_zone, color_chooser, "Choisissez votre couleur", JOptionPane.PLAIN_MESSAGE);
			  		// on change le curseur
			  		this.setFillingCursor();
		  		}
		  		break;

	  		case Configuration.TRIANGLE:
	  			this.drawing_zone.setBuildingFigure(new OneTriangle(Color.black, false));
		  		break;

		  	case Configuration.SQUARE:
	  			this.drawing_zone.setBuildingFigure(new OneRectangle(Color.black, false));
		  		break;

		  	case Configuration.CIRCLE:
		  		this.drawing_zone.setBuildingFigure(new OneCircle(Color.black, false));
		  		break;

		  	case Configuration.POLYGON:
		  		this.drawing_zone.setBuildingFigure(new OnePolygon(Color.black, false));
		  		break;

		  	case Configuration.DELETE:
		  		// recupere la figure selectionnée
		  		FigureGeom figure = this.drawing_zone.getSelectedFigure();

		  		// verifie que la figure selectionnée est non null
            	if(figure != null){
            		// supprime la figure de l'affichage
            		this.drawing_zone.deleteFigure(figure);
            	}
		  		break;

		  	default:
		  		break;
	  	}
  	}

	/**
	 * Méthode qui transforme le curseur en pinceau
	 */
	private void setFillingCursor() {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image cursor_icone = toolkit.getImage("../img/cursor-filling.png");
		Cursor c = toolkit.createCustomCursor(cursor_icone, new Point(0, 31), "cursor-filling");
		drawing_zone.setCursor(c);
	}

}
