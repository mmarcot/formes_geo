import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;


/**
 * Classe auditrice qui gère les interactions avec le JPanel 'DrawPanel'
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
public class DrawPanelListener extends AbstractListener implements MouseListener, MouseMotionListener {
    
	// dernier point (origine)
    private Point last_point = null;
    // l'index de l'inputbutton (PointArea) trouvé
    private int resizing_point_index;
    // coordonnées de chaque click de pressed
    private Point pressed_coordonates;

    // le type d'opération sur la figure courante
    private int type_operation = DrawPanelListener.NONE;
    // types d'opérations
    final private static int NONE = -1;
    final private static int TRANSLATION = 0;
    final private static int RESIZING = 1;
    
    // methodes non surchargées :
    public void mouseExited(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}    
    
    /**
     * Methode qui transforme le cursor lorsqu'il passe sur un PointArea (carrés de redimensionnement)
     */
    public void mouseMoved(MouseEvent e) {
    	// derniere position de la souris :
    	Point current = (Point) e.getPoint();
    	
        DrawPanel panel = (DrawPanel) e.getSource();
        FigureGeom figure = panel.getSelectedFigure();
        
        if (figure != null && AbstractListener.getMode() == AbstractListener.EDIT_MODE) {
        	// On récupère les PointArea de la figure
	        ArrayList<PointArea> list_points_area = figure.getInputPoints();
	    	
	        boolean point_area_found = false;
			// On parcourt les PointArea de la figure et on vérifie si la souris se trouve sur l'un d'eux
	        // Si oui, on change de curseur
	        for (int i = 0; i < list_points_area.size() && !point_area_found; i++) {
	            if (list_points_area.get(i).contains(current)) {
	            	panel.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
	                point_area_found = true;
	            } else {
	            	if (AbstractListener.getMode() != AbstractListener.FILLING_MODE) {
	            		panel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	            	}
	            }
	        }
        }
    }
    
	/**
	 * Gère la translation d'une figure entière et la translation d'un
	 * point de type PoitArea en particulier
	 */
	@Override
    public void mouseDragged(MouseEvent e) {
        if (this.type_operation != DrawPanelListener.NONE) {
            Point current = (Point) e.getPoint();
            DrawPanel panel = (DrawPanel) e.getSource();

            // on regarde si le dragged est appliqué sur une figure :
            FigureGeom figure = this.detectContainingFigure(panel.getTabFigures(), this.pressed_coordonates, panel);

            if (figure != null) {
                int move_x, move_y;
                // Click à l'intérieur de la figure (figure != null)
                move_x = (int) (current.getX() - this.last_point.getX());
                move_y = (int) (current.getY() - this.last_point.getY());

                this.last_point.setLocation(current.getX(), current.getY());

                if (this.type_operation == DrawPanelListener.TRANSLATION) {
                    figure.translate(move_x, move_y);
                } else if (this.type_operation == DrawPanelListener.RESIZING) {
                    figure.setPoint(this.resizing_point_index, move_x, move_y);
                }
                panel.repaint();
            }
        }
    }

	/**
	 * Gère les clics pour :
	 * 	- ajouter des points à une figure
	 * 	- selectionner une figure sur le JPanel
	 * 	- remplir une figure de couleur
	 */
	@Override
    public void mouseClicked(MouseEvent e) {
		DrawPanel panel = (DrawPanel) e.getSource();
        Point point = (Point) e.getPoint();
        FigureGeom figure;
        int mode = AbstractListener.getMode();

        // On ne valide l'entrée en mode édition que si L'objet Figure est crée
        // dans ToolbarListener ET que la figure n'est pas encore compète
        if (mode == AbstractListener.EDIT_MODE) {
            if ((figure = panel.getBuildingFigure()) != null && !figure.isCompleted()) {
                figure.addPoint(point);

                // Si la figure est complète, sortie du mode édition, et ajout de la figure
                // à la liste de figure du panel >>> et on la met en sélectionnée <<<
                if (figure.isCompleted()) {
                    AbstractListener.resetMode();
                    panel.addFigure(figure);
                    panel.setFigureAsSelected(figure);
                }
            }
        } else if (mode == AbstractListener.FILLING_MODE) {
            figure = panel.getSelectedFigure();
            Color color = panel.getChosenColor();

            if (figure != null) {
                figure.setColor(color);
                figure.setFilled(true);
            }
        }

        panel.repaint();
    }

    /**
     * Gère la sélection des figure 
     * et le changement de curseur sur les carré de redimensionnement
     */
    public void mousePressed(MouseEvent e) {
        ArrayList<PointArea> points_area;
        boolean point_area_found = false;

        Point current = (Point) e.getPoint();
        this.pressed_coordonates = current;
        DrawPanel panel = (DrawPanel) e.getSource();

        // On détecte la figure contenant le point cliqué
        FigureGeom figure = this.detectContainingFigure(panel.getTabFigures(), current, panel);

        // Si figure trouvée, on place cette figure en tant que figure active dans le panel
        this.last_point = current;

        if (figure != null) {
            panel.setFigureAsSelected(figure);
            points_area = figure.getInputPoints();
            
            // On parcourt les PointArea de la figure et on vérifie si la souris se trouve sur l'un d'eux
            // Si oui, on passe en mode resizing
            for (int i = 0; i < points_area.size() && !point_area_found; i++) {
                if (points_area.get(i).contains(current)) {
                    this.type_operation = DrawPanelListener.RESIZING;
                    this.resizing_point_index = i;
                    point_area_found = true;
                }
            }

            if (!point_area_found && figure.contains(current)) {
                this.type_operation = DrawPanelListener.TRANSLATION;
            }
        }
    }

    /**
     * Methode qui sauvegarde l'état du JPanel pour l'implémentation de la fonctionnalité undo/redo
     */
	public void mouseReleased(MouseEvent e) {

    	if( AbstractListener.getMode() != AbstractListener.EDIT_MODE ) {
    		((DrawPanel)e.getSource()).saveCurrentStatement();
    		((DrawPanel)e.getSource()).setTabIndex(0);
    	}
	}

    /**
     * Methode qui renvoit la figure contenant le point donné en paramètre
     */
    private FigureGeom detectContainingFigure(ArrayList<FigureGeom> tab_figure, Point p, DrawPanel panel) {
        FigureGeom figure, current;
        ArrayList<PointArea> points_area;
        figure = null;
        boolean detected = false;

        if (tab_figure.size() > 0) {
            // boucle sur les figures du panel
            for (int i = (tab_figure.size() - 1); i >= 0 && !detected; i--) {
                current = tab_figure.get(i);
                if (current.contains(p)) {
                    figure = current;
                    detected = true;
                }
                
                // boucle aussi sur les carrés de sélection 
                // (pour qu'un clic sur un carré de selection soit aussi pris en compte)
                points_area = current.getInputPoints();
                for (int j = 0; j < points_area.size(); j++) {
                    if (points_area.get(j).contains(p)) {
                        figure = current;
                        detected = true;
                    }
                }
            }
        }
        return figure;
    }
    
}
