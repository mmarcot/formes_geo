import java.awt.Color;
import java.awt.Point;


/**
 * Classe permettant de générer des cercles
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
@SuppressWarnings("serial")
public class OneCircle extends FigureGeom {

	/**
	 * constructeur d'un cercle
	 */
	public OneCircle(Color c, boolean fill) {
		super(c, fill);
		this.nb_point_final = 2;
	}
	
	/**
	 * constructeur par copie
	 */
	public OneCircle(FigureGeom f) {
		super(f);
		this.nb_point_final = 2;
	}

	/**
	 * Permet de savoir si un point est contenu dans une figure
	 * @param point le point à localiser
	 */
	@Override
	public boolean contains(Point test) {
		boolean res = false;
		
		if (!saved_points.isEmpty()) {
			int xo = saved_points.get(0).x;
			int yo = saved_points.get(0).y;
			int xa = saved_points.get(1).x;
			int ya = saved_points.get(1).y;

			int rayon = (int) Math.sqrt( Math.pow((xo-xa), 2) + Math.pow((yo-ya), 2) );

			// distance entre le point à tester et l'origine du cercle :
			int distance_o = (int) Math.sqrt( Math.pow((xo-test.x), 2) + Math.pow((yo-test.y), 2) );

			if (distance_o <= rayon) {
				res = true;
			}
		} else {
			res = false;
		}
		return res;
	}

}