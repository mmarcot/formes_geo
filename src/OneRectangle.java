import java.awt.Color;
import java.awt.Point;


/**
 * Classe permettant de générer des rectangles
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
@SuppressWarnings("serial")
public class OneRectangle extends OnePolygon {
	/**
	 * constructeur d'un rectangle
	 */
	public OneRectangle(Color c, boolean fill) {
		super(c, fill);
		this.nb_point_final = 2;
	}
	
	/**
	 * constructeur par copie
	 */
	public OneRectangle(FigureGeom f) {
		super(f);
		this.nb_point_final = 2;
	}

	/**
	 * Methode qui va générer les points de memorisation en fonction
	 * des input points
	 */
	@Override
	protected void genSavedPoints() {
		// on vide la liste :
		this.saved_points.clear();
		
		// 1er saved_point
		PointArea p1 = this.input_points.get(0);
		this.saved_points.add(new Point(p1.x, p1.y));
		
		//2e saved_point
		// on memorise le 2e input_point
		PointArea p2 = this.input_points.get(1);
		this.saved_points.add(new Point(p2.x, p1.y));
		
		// 3e saved_point
		this.saved_points.add(new Point(p2.x, p2.y));
		
		// 4e saved_point
		this.saved_points.add(new Point(p1.x, p2.y));
	}

}

