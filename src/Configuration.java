
/**
 * Classe de configuration permettant de centraliser la configuration 
 * et des constantes 
 * @author Guillaume Penaud, Nicolas Phung, Yann Lalaoui, Mallory Marcot
 */
public class Configuration {

	// etats du mode débug pour chaque classe
	final public static boolean DEBUG_TOOL_BAR_LISTENER = 	false;
	final public static boolean DEBUG_DRAW_PANEL = 			false;
	final public static boolean DEBUG_DRAW_PANEL_LISTENER = false;

	/**
	 *  Taille en pixels d'un carré de selection de point
	 */
	public static final int POINTAREA_SIZE = 10;

	/**
	 * Dimensions initiales de la zone de dessin
	 */
	public static final int DRAW_PANEL_WIDTH = 500;
	public static final int DRAW_PANEL_HEIGHT = 500;

	/**
	 * Taille du tableau de listes, permettant de faire des undo/redo
	 */
	public static final int SIZE_UNDO_REDO = 10;

	/**
	 * Nom de la fenêtre de l'application
	 */
	public static String WINDOW_NAME = "Paint Révolution : Java";

	/**
	 * Constantes d'identification des bouttons
	 */
	final public static String BACK_TO_SELECT_MODE = "select";
	final public static String TRIANGLE = "triangle";
	final public static String SQUARE = "square";
	final public static String CIRCLE = "circle";
	final public static String POLYGON = "polygon";
	final public static String COLORS = "colors";
	final public static String COPY = "copy";
	final public static String TRANSPARENT = "transparent";
	final public static String DELETE = "delete";

	/**
	 * Constantes d'identification du JMenu
	 */
	public final static String SAVE = "Sauvegarder";
	public final static String LOAD = "Charger";
	public final static String EXIT = "Quitter";
	public final static String UNDO = "Annuler";
	public final static String REDO = "Refaire";
	public final static String A_PROPOS = "A propos";
	public final static String VERSION = "Version";
	public final static String UPDATE = "Mettre à jour";
	public final static String EXPORT = "Exporter en JPG";

	/**
	 * Tooltips des boutons
	 */
	public final static String FILL_TOOLTIP = "Choisir une couleur de remplissage";
	public final static String TRANSP_TOOLTIP = "Couleur : transparent";
	public final static String COPY_TOOLTIP = "Copier la figure sélectionnée";
	public final static String DELETE_TOOLTIP = "Effacer la figure";
	public final static String POLYGON_TOOLTIP = "Dessiner un polygone (4 points et plus)";
	public final static String TRIANGLE_TOOLTIP = "Dessiner un triangle (3 points)";
	public final static String CIRCLE_TOOLTIP = "Dessiner un cercle (2 points)";
	public final static String RECTANGLE_TOOLTIP = "Dessiner un rectangle (2 points)";
	
	/**
	 * Messages à l'utilisateur
	 */
	public final static String SAVED_MESS = "Dessin sauvegardé !";
	public final static String ERR_LOAD = "Le fichier de sauvegarde n'est pas valide";
	public final static String ABOUT_MES = "Projet Paint Révolution realisé par :\nYann Lalaoui\nMarcot Mallory\nPenaud Guillaume\nPhung Nicolas";
	public final static String VERS_MES = "Version 2.4";
	public final static String UPDATE_MES = "Logiciel déjà à jour !!!";
}
