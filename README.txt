######## README Projet Java Formes Geom ########

--- Conventions de codage ---
casse
=====
	 - variables: 	ma_super_variable
	 - méthodes: 	maSuperMethode
	 - classes:   	MaSuperClasse

Le code DOIT être en anglais.
Les commentaires peuvent être en français.
On commente toutes les méthodes de façon à avoir une javadoc clean.

Dans le code lui même, on commente uniquement des choses non-évidentes !

exemple de methode type
=======================

/**
 * incrémente de 5
 *
 * @param ma_super_variable
 * @return ma_super_variable_incrementee
 */
public int maSuperMethode(int ma_super_variable) {
	// inutile de commenter ce genre de traitement débile
	return (ma_super_variable + 5);
} 

exemple de if
=============

if (something) {
// commentaire concernant la condition if
	// commentaire sur ma_super_variable_1
	int ma_super_variable_1;

	// commentaire sur ma_super_variable_2
	int ma_super_variable_2;
} else if (other) {
	
} else {
	
}

exemple de for
==============

for (int i = 0; i < size; i++) {
// commentaire concernant la boucle for
	// commentaire sur ma_super_variable_2
	int ma_super_variable_2;
}